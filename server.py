import datetime
from http.server import HTTPServer, BaseHTTPRequestHandler

class MyRequestHandler(BaseHTTPRequestHandler):
    def do_GET(self):
        self.send_response(200)
        self.send_header("Content-type", "text/html")
        self.end_headers()

        html = datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')

        self.wfile.write(html.encode('utf-8'))

print('start')
server = HTTPServer(('0.0.0.0', 10000), MyRequestHandler)
server.serve_forever()
