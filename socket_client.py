import socket

my_socket = socket.socket()
host = '127.0.0.1'
port = 10001

my_socket.connect((host, port))
bytes_received = my_socket.recv(1024)
print('This is what we received from server : [%s]' % (bytes_received))
my_socket.close()
