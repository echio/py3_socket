import socket

def now_string():
  import datetime
  return datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")

my_socket = socket.socket()
# host = socket.gethostname()
host = '127.0.0.1'
port = 10001
print('Host = [%s], Port = [%s]' % (host, port))
my_socket.bind((host, port))

backlog = 5
my_socket.listen(backlog)
while True:
  connection, address = my_socket.accept()
  print('Connection from [%s]' % (str(address)))

  message = 'Server response: [%s]' % (now_string())
  connection.send(bytes(message, 'ascii'))
  connection.close()

